<h1 align='center'> <img src="https://i0.wp.com/espirittech.com/wp-content/uploads/2022/05/asp-net2.jpg?resize=768%2C246&ssl=1" width="600" height="200"> </h1>


<h1 align='center'> Goktug Erol</h1>


<h4 align='center'>

<h2 align='center'> Full-Stack Software Engineer | Enterprise & Government Applications </h2>
<h4 align='center'>


[E-Mail](cyber9unk@outlook.jp)
<!-- [Website](legacycode.dev) </h4> -->

Welcome to my GitHub profile! I'm Goktug Erol, a highly skilled full-stack software engineer with a focus on developing enterprise and governmental applications. I have a passion for leveraging the power of technology to solve complex challenges and drive business growth.


<h4 align='center'>
	
[![GitHub Streak](https://streak-stats.demolab.com?user=goktugerol-dev&theme=dark-smoky&date_format=%5BY%20%5DM%20j)](https://git.io/streak-stats)

[![Goktug's GitHub stats](https://github-readme-stats.vercel.app/api?username=goktugerol-dev&count_private=true&show_icons=true&theme=dark-smoky)](https://github.com/anuraghazra/github-readme-stats)
</h4>

---
### Technical Proficiencies

- Microsoft Ecosystem: .NET Core, ASP.NET Core, C#, Entity Framework, Razor, MVC
- MEAN & MEVN Stack
- Python
- Laravel & PHP
- Agile Methodologies

### Experience and Expertise

With a solid background in software architecture, software testing, and DevOps, I have successfully delivered web and mobile applications using a diverse range of technologies. My expertise lies in building scalable and secure solutions within the Microsoft ecosystem, and I am experienced in leveraging the power of Azure.

### Collaboration and Project Success

I strongly believe in the power of collaboration and agile methodologies. Currently, I am employed as a full-stack software engineer at a prominent software factory in Mexico, where I work closely with a team of seasoned developers and engineers. Together, we deliver high-quality enterprise-level web and mobile applications for government agencies and large enterprises.

### Continuous Learning and Innovation

I am dedicated to continuous learning and staying up-to-date with industry trends. My commitment to professionalism and delivering high-quality work enables me to provide innovative and efficient solutions to complex software challenges. I am constantly seeking opportunities to enhance my knowledge and skills, ensuring that I can deliver the best possible outcomes for my clients.

Let's connect and collaborate on exciting projects. Feel free to explore my repositories and reach out to me for any inquiries or opportunities.
---


--------


<h4 align='center'>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=goktugerol-dev&langs_count=10&count_private=true&theme=chartreuse-dark)](https://github.com/goktugerol-dev/github-readme-stats) 

</h4>


<h1 align='center'>Technology Stack</h1>

<h5> Programming Languages </h5>

![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white)
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)
![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white)
![TailwindCSS](https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white)



<h5> Frameworks & Technologies</h5>

![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white)
![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white)
![Vue.js](https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D)
![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)
![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)
![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white)
![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)

<h5> Design Stack </h5>

![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)
![Adobe Photoshop](https://img.shields.io/badge/adobe%20photoshop-%2331A8FF.svg?style=for-the-badge&logo=adobe%20photoshop&logoColor=white)
![Adobe Lightroom](https://img.shields.io/badge/Adobe%20Lightroom-31A8FF.svg?style=for-the-badge&logo=Adobe%20Lightroom&logoColor=white)
![Adobe Illustrator](https://img.shields.io/badge/adobe%20illustrator-%23FF9A00.svg?style=for-the-badge&logo=adobe%20illustrator&logoColor=white)

<hr>
	
	
<h5> Database Technologies </h5>

![MicrosoftSQLServer](https://img.shields.io/badge/Microsoft%20SQL%20Server-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)

<h5> Cloud Technologies </h5>

![DigitalOcean](https://img.shields.io/badge/DigitalOcean-%230167ff.svg?style=for-the-badge&logo=digitalOcean&logoColor=white)
![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white)
![Azure](https://img.shields.io/badge/azure-%230072C6.svg?style=for-the-badge&logo=microsoftazure&logoColor=white)

<hr>

<h5> Operating Systems </h5>
	
![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)
![Chrome OS](https://img.shields.io/badge/chrome%20os-3d89fc?style=for-the-badge&logo=google%20chrome&logoColor=white)
![Debian](https://img.shields.io/badge/Debian-D70A53?style=for-the-badge&logo=debian&logoColor=white)
![Kali](https://img.shields.io/badge/Kali-268BEE?style=for-the-badge&logo=kalilinux&logoColor=white)
![Arch](https://img.shields.io/badge/Arch%20Linux-1793D1?logo=arch-linux&logoColor=fff&style=for-the-badge)
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)

<h5> Coding Challenges - Click to See: </h5>

[![Codewars](https://img.shields.io/badge/Codewars-B1361E?style=for-the-badge&logo=codewars&logoColor=grey)](https://www.hackerrank.com/ge_dev)
[![HackerRank (https://www.hackerrank.com/ge_dev)](https://img.shields.io/badge/-Hackerrank-2EC866?style=for-the-badge&logo=HackerRank&logoColor=white)](https://www.codewars.com/users/ge_dev)
	

------

<h3 align='center'> Blog </h3>
<h5 align='center'>
	
[![Dev.to blog](https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=dev.to&logoColor=white)](https://dev.to/goktugerol)
[![Twitter: Goktug Erol](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/the_code_father/)

</h5>
<h3 align='center'> Community Member</h3>
<h5 align='center'>

![Discord](https://img.shields.io/badge/Discord-%235865F2.svg?style=for-the-badge&logo=discord&logoColor=white)
![Slack](https://img.shields.io/badge/Slack-4A154B?style=for-the-badge&logo=slack&logoColor=white)


- [Python en Español](https://hablemospython.dev/index.html)
- [Real Python](https://realpython.com/)
- [Python Discord](https://www.pythondiscord.com/)
- [Replit](https://replit.com/)
- [Java Discord](https://javadiscord.net/)

- [Carbon Language](https://github.com/carbon-language/carbon-lang)
- [Official Rust Community](https://www.rust-lang.org/)
</h5>
